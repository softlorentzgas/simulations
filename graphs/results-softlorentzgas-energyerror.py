# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 8:17:42 2019

figure for results-softlorentzgas-energyerror

@author: Arsku
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

algorithms = ["boris", "2", "rk4", "yhe2", "chin_2a", "chin_2b", 
              "suzuki4", "yoshida4", "prk4", "prk6", "ss6_7", "ss6_9",
              "ss8_15", "ss8_17"]

data = pd.read_csv('energyerror_soft.csv', header=None)

# number of time steps in data
num_ts = 5

magfields = int(data.shape[1] / num_ts)

sns.set_palette(sns.color_palette("hls", len(algorithms)))


for i in range(magfields):

    energy = data.iloc[:,num_ts*i:num_ts*(i+1)]
    energy = pd.DataFrame(energy.values.reshape((5,14)))
    energy.columns=algorithms


    f = plt.figure()

    plt.semilogy(energy, 'o-')
    plt.legend(algorithms, bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
    plt.xticks(np.arange(num_ts), ['1e-2', '1e-3', '1e-4', '1e-5', '1e-6'])

    f.savefig('energy_errors_%d.pdf' % i, bbox_inches='tight')