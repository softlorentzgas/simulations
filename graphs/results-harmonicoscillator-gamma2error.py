# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 15:24:48 2018

figure for results-harmonicoscillator-gamma2error

@author: Arsku
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

algorithms = ["boris", "2", "rk4", "yhe2", "chin_2a", "chin_2b", 
              "suzuki4", "yoshida4", "prk4", "prk6", "ss6_7", "ss6_9",
              "ss8_15", "ss8_17"]

data = pd.read_csv('gamma2.csv', header=None)

# dataframes setup: algorithms gamma2-values as columns
gamma = pd.DataFrame(data.values.reshape((5,14)))
gamma.columns=algorithms

f = plt.figure()

sns.set_palette(sns.color_palette("hls", 14))

plt.semilogy(gamma, 'o-')
plt.legend(algorithms, bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
plt.xticks(np.arange(5), ['1e-2', '1e-3', '1e-4', '1e-5', '1e-6'])

f.savefig('gamma2_errors.pdf', bbox_inches='tight')