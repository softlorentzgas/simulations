# -*- coding: utf-8 -*-
"""
Created on Tue Jan 15 13:50:39 2019

figure for results-harmonicoscillator-gamma2error

@author: Arsku
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

algorithms = ["boris", "2", "rk4", "yhe2", "chin_2a", "chin_2b", 
              "suzuki4", "yoshida4", "prk4", "prk6", "ss6_7", "ss6_9",
              "ss8_15", "ss8_17"]

data = pd.read_csv('energyerror.csv', header=None)

# dataframes setup: algorithms gamma2-values as columns
energy = pd.DataFrame(data.values.reshape((5,14)))
energy.columns=algorithms

f = plt.figure()

sns.set_palette(sns.color_palette("hls", 14))

plt.semilogy(energy, 'o-')
plt.legend(algorithms, bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
plt.xticks(np.arange(5), ['1e-2', '1e-3', '1e-4', '1e-5', '1e-6'])

f.savefig('energy_errors.pdf', bbox_inches='tight')