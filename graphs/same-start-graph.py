# -*- coding: utf-8 -*-
"""
Created on Tue Jan 15 16:29:11 2019

@author: Arsku
"""

import h5py
import matplotlib.pyplot as plt

algos = ['yhe2', 'rk4', 'chin_2a']
mag = 'mag2'
end = 2100
xmin = -0.5
xmax = 4.5
ymin = -2
ymax = 2

fig = plt.figure()

for algo in algos:
    filename = algo + mag + '.h5'
    f = h5py.File(filename, 'r')
    
    traj = f["/trajectories/particle1"].value
    
    xtraj, ytraj = zip(*traj)
    
    plt.subplot(1, len(algos), algos.index(algo) + 1)
    plt.plot(xtraj[:end], ytraj[:end], 'r', xtraj[0], ytraj[0], 'or')
    plt.axis([xmin,xmax,ymin,ymax])
    if algos.index(algo) > 0:
        plt.yticks([])
    
    f.close()
    
    
fig.savefig('same-start-graph.pdf')