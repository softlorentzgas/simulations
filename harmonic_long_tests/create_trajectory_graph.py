# -*- coding: utf-8 -*-
"""
Created on Sat Jun 23 12:41:03 2018

@author: Ariane
"""
import h5py
import numpy as np


file = open('draw_all_trajectories.py','w')

file.write('import matplotlib.pyplot as plt\n')


file.write('\n')

np.set_printoptions(threshold=np.nan)
file.write('\n')

algorithms = ["boris", "2", "rk4", "yhe2", "chin_2a", "chin_2b", 
              "suzuki4", "yoshida4", "prk4", "prk6", "ss6_7", "ss6_9",
              "ss8_15", "ss8_17"]
n = 1 

for algorithm in algorithms:
    
    
    for i in range(2,7):
    
        filepath = 'rdata/' + algorithm + '1e-' + str(i) + '.h5'
        f=h5py.File(filepath, 'r')

        # gets list of tuples  [(x0,y0), (x1,y1), ...] cast into np-array (.value)
        traj = f['/trajectories/particle1'].value

        f.close()

        namex = "x" + str(i) + algorithm  
        namey = "y" + str(i) + algorithm 
            
        datax = np.array2string(traj[:,0], separator=",")
        datay = np.array2string(traj[:,1], separator=",")
            
        file.write(namex + "=" + datax + "\n")
        file.write('\n')
        file.write(namey + "=" + datay + "\n")
        file.write('\n')
            
        file.write('plt.figure(' + str(n) + ')\n')
        file.write('plt.plot(' + namex + ',' + namey + ')\n')
        file.write('plt.title(\'' + algorithm + '\')\n')
        file.write('plt.axis(\'equal\')\n')
        file.write('plt.xlabel(\'x\')\n')
        file.write('plt.ylabel(\'y\')\n')
        file.write('plt.savefig(\'' + algorithm + '1e-' + str(i) + '.pdf\')\n')
        n+=1        
            

file.close()        
