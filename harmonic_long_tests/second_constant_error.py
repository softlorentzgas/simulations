# -*- coding: utf-8 -*-
"""
implementation of 
https://ris.utwente.nl/ws/portalfiles/portal/6766733/Geurts90chaotic.pdf
"""

import h5py
import numpy as np
import csv

algorithms = ["boris", "2", "rk4", "yhe2", "chin_2a", "chin_2b", 
              "suzuki4", "yoshida4", "prk4", "prk6", "ss6_7", "ss6_9",
              "ss8_15", "ss8_17"]



gamma = []

# loop over algorithm
for algorithm in algorithms:
    listofgamma = []
    for i in range(2,7):
        filename = 'rdata/' + algorithm +  '1e-' + str(i) + '.h5'
        f=h5py.File(filename,'r')
        print(filename)
        #setup
        e = 1
        B = f['parameters'].attrs.get('B_field')
        m = 1
        ekin = np.array(f['kinetic_energy'])
        epot = np.array(f['potential_energy'])
        esum =  ekin[-1] + epot[-1]
        omega = f['parameters'].attrs.get('potential_strength')
        alpha = -e * B / (m * omega)
        K_red = np.sqrt(2 * esum / m)    #according to Janne   
        
        # get energies
        traj_data=f["/trajectories/particle1"]
        traj = traj_data.value
        x = traj[-1][0]
        y = traj[-1][1]
        vel_data=f['/velocities/particle1']
        vel = vel_data.value
        vx = vel[-1][0]
        vy = vel[-1][0]
        
        x0 = traj_data[0][0]
        y0 = traj_data[0][1]
        vx0 = vel_data[0][0]
        vy0 = vel_data[0][1]
        
        y1 = x / K_red *omega
        y2 = y / K_red *omega
        y1_dot = vx / K_red
        y2_dot = vy / K_red
        c2 = 2*(y1*y2_dot - y2*y1_dot) - alpha*(y1*y1 + y2*y2)
        
        y1 = x0 / K_red *omega
        y2 = y0 / K_red *omega
        y1_dot = vx0 / K_red
        y2_dot = vy0 / K_red
        c2_bm = 2*(y1*y2_dot - y2*y1_dot) - alpha*(y1*y1 + y2*y2)
        
        gamma0 = np.absolute(c2-c2_bm)/c2_bm
        listofgamma.append(gamma0)
        f.close()
        
        
    gamma.append(listofgamma)
        
        



file = open('gamma2.csv','wb')

with file:
    writer = csv.writer(file)
    for algorithm in range(len(gamma)):    
        writer.writerow(gamma[algorithm])        
#        writer.writerow(gamma)
file.close()
