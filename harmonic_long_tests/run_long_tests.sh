#!/bin/bash

energy=3
magfield=5
potential=3
maxt=1000

algo=("boris" "chin_2a" "chin_2b" "yhe2" "rk4" "prk4" "prk6" "suzuki4" "yoshida4" "2" "ss6_7" "ss6_9" "ss8_15" "ss8_17")

a=0
while (( $a < 14 ))
do
        b=2
        echo ${algo[${a}]}
        while (( $b < 7 ))
        do
		while ((`pgrep -c bill2d`>19))
		do
			sleep 1
		done
		bill2d --billtype 4 --save-trajectory --save-velocity --save-energies --table 0 --sparsesave $((10**${b})) --potential-strength ${potential} --propagator ${algo[${a}]} --max-t ${maxt} --energy ${energy} --magnetic-field ${magfield} --output 0 --delta-t 1e-${b} --savepath rdata/${algo[${a}]}1e-${b}.h5 &

                
                echo 1e-${b}/${algo[${a}]}.h5
		(( b+=1))
        done
        ((a+=1))
done

while ((`pgrep -c bill2d`>0))
do
	sleep 1 
done
echo Done! 
