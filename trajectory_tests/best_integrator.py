# -*- coding: utf-8 -*-
"""
Analysis of the trajectories. 

Counting how often a certain algorithm wins the 'vote' for best trajectory

"""

import pandas as pd
import numpy as np

algorithms = ["boris", "2", "rk4", "yhe2", "chin_2a", "chin_2b", 
              "suzuki4", "yoshida4", "prk4", "prk6", "ss6_7", "ss6_9",
              "ss8_15", "ss8_17"]

header = ['comp', 'divtime', 'exact', 'magfield', 'run', 'ts']

num = np.shape(algorithms)[0]

# Load file from divergence.py into dataframe
df = pd.read_csv('divtime.txt', sep = ' ', header = 0)

# sort according to experiment's variables
sorteddf = df.sort_values(['magfield', 'exact', 'ts', 'run'])

rows = np.shape(df)[0]
brackets = round(rows / num)
bestintegrator = pd.DataFrame(columns=header)

for i in range(brackets):
    cache = sorteddf[i*num:(i+1)*num]
    divmax = cache['divtime'].idxmax()
    compname = cache.loc[divmax]['comp']
    # exclude runs, that explode right away (diverged already after the first 
    # observed step)
    if divmax is not 1:
        bestintegrator = bestintegrator.append(cache.loc[divmax], ignore_index = True)

#bestintegrator['comp'].value_counts()

#%%
for i in range(2,7):
    for j in range(5):
        for k in range(7):
            countdf = bestintegrator.loc[(bestintegrator['ts'] == i) & (bestintegrator['magfield'] == k) & (bestintegrator['run'] == j) ]
            print('ts: ', i, ', run: ', j, ', mag: ', k)
            print(countdf['comp'].value_counts())
#%%
            
median = df.groupby(['magfield', 'ts', 'comp']).median()
maximum = df.groupby(['magfield', 'ts', 'comp']).max()
minimum = df.groupby(['magfield', 'ts', 'comp']).min()

