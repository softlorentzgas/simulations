# -*- coding: utf-8 -*-
"""
A script to compare cheaper algorithms trajectories to the expected more exact
trajectories. 
Input: files made by run_trajectory_tests.sh
Output: one file per algorithm and comparative algoritm
"""

import h5py
import numpy as np

np.set_printoptions(threshold=np.nan) 

magfield = [0.2, 0.335, 0.5, 1]

algorithms = ["boris", "2", "rk4", "yhe2", "chin_2a", "chin_2b", 
              "suzuki4", "yoshida4", "prk4", "prk6", "ss6_7", "ss6_9",
              "ss8_15", "ss8_17"]

for magfieldvalue in range(4):
    
    for algorithm in algorithms:
        
        # get "exact" trajectory
        filepath = "1e-7/" + algorithm + "mag" + str(magfieldvalue) + ".h5"
        print(filepath)
        f = h5py.File(filepath, 'r')
        
        trajexact = f['/trajectories/particle1'].value
        velexact = f['/velocities/particle1'].value
        
        f.close()
        
        #loop over less exact runs with all algorithms
        for compalgo in algorithms:

            writepath = "analysis/" + algorithm + compalgo + "mag" + \
            str(magfield[magfieldvalue]) + ".py"
            file = open(writepath, 'w')
            file.write("import numpy as np\n")
            file.write("import matplotlib.pyplot as plt\n\n")
            

            for timestep in range(2,7):
                filepath = "1e-"+ str(timestep) + "/" + compalgo + "mag" + \
                str(magfieldvalue) + ".h5"
                g = h5py.File(filepath, 'r')
                
                comptraj = g['/trajectories/particle1'].value
                compvel = g['/velocities/particle1'].value
                
                g.close()
                
                # numpy power: elementwise power -> outcome should be a vector 
                # of the same length as the origial vectors
                delta = np.power((np.power((trajexact[:,0]-comptraj[:,0]),2) + \
                         np.power((trajexact[:,1]-comptraj[:,1]),2) + \
                         np.power((velexact[:,0]-compvel[:,0]),2) + \
                         np.power((velexact[:,1]-compvel[:,1]),2)),(0.5))

                deltawrite = np.array2string(delta, separator=",")
                file.write("traj1e" + str(timestep) + "=" + deltawrite + "\n\n")
                
                
            file.write("t = np.linspace(0,50,5000)\n")
            file.write("plt.plot(t,traj1e2, label=\'1e-2\')\n")
            file.write("plt.plot(t,traj1e3, label=\'1e-3\')\n")
            file.write("plt.plot(t,traj1e4, label=\'1e-4\')\n")
            file.write("plt.plot(t,traj1e5, label=\'1e-5\')\n")
            file.write("plt.plot(t,traj1e6, label=\'1e-6\')\n")
            title = compalgo + " trajectories compared to exact " + algorithm
            file.write("plt.title('" + title + "\')\n")
            file.write("plt.xlabel(\'Simulation time in au\')\n")
            file.write("plt.ylabel(\'difference in trajectories\')\n")
            file.write("plt.legend(loc=\'upper left\')\n")
            file.write("plt.savefig(\'" + algorithm + compalgo + \
                                    str(magfieldvalue) + ".pdf\')\n")
            file.close()



