# -*- coding: utf-8 -*-
"""
A script to compute the time at which the different time steps of algorithms 
diverge from each other. 
Input: files made by run_trajectory_tests.sh
Output: 
"""

import h5py
import numpy as np
import pandas as pd

magfield = [0.2, 0.335, 0.5, 1, 1.2, 1.5, 2]

algorithms = ["boris", "2", "rk4", "yhe2", "chin_2a", "chin_2b", 
              "suzuki4", "yoshida4", "prk4", "prk6", "ss6_7", "ss6_9",
              "ss8_15", "ss8_17"]

           

limit = 1e-6

exactalgolist = []
compalgolist = []
magfieldlist = []
timesteplist = []
divtimelist = []
runlist = []

for magfieldvalue in range(7):
    
    for run in range(5):
        
        if magfieldvalue < 4:
            folder = "runs/run"
            suffix = "mag" + str(magfieldvalue) + ".h5"
        else:
            folder = "biggerb/run"
            suffix = "mag" + str(magfieldvalue - 4) + ".h5"
        
        prefix = folder + str(run + 1) + "/"
        
        
        
        for algorithm in algorithms:
            
            # get "exact" trajectory
            filepath = prefix + "1e-7/" + algorithm + suffix
            f = h5py.File(filepath, 'r')
            
            trajexact = f['/trajectories/particle1'].value
            velexact = f['/velocities/particle1'].value
            
            f.close()
            
            #loop over less exact runs with all algorithms
            for compalgo in algorithms:
    
                for timestep in range(2,7):
    
                    filepath = prefix + "1e-"+ str(timestep) + "/" + compalgo + \
                    suffix
                    g = h5py.File(filepath, 'r')
                    
                    comptraj = g['/trajectories/particle1'].value
                    compvel = g['/velocities/particle1'].value
                    
                    g.close()
                    
                    # numpy power: elementwise power -> outcome should be a vector 
                    # of the same length as the origial vectors
                    delta = np.power((np.power((trajexact[:,0]-comptraj[:,0]),2) + \
                             np.power((trajexact[:,1]-comptraj[:,1]),2) + \
                             np.power((velexact[:,0]-compvel[:,0]),2) + \
                             np.power((velexact[:,1]-compvel[:,1]),2)),(0.5))
    
                    divtime = np.argmax(delta>limit)
                    
                    exactalgolist.append(algorithm)
                    compalgolist.append(compalgo)
                    magfieldlist.append(magfieldvalue)
                    timesteplist.append(timestep)
                    divtimelist.append(divtime)
                    runlist.append(run)
                    
            
data = {'exact': exactalgolist, 'comp': compalgolist, 'magfield': magfieldlist,
     'ts': timesteplist, 'divtime': divtimelist, 'run': runlist}
df = pd.DataFrame(data)                

df.to_csv(r'divtime.txt', index=False, sep=' ', mode='w')

                
                
                
                
