# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 17:17:45 2019

@author: Arsku
"""

import h5py
import numpy as np
import csv

algorithms = ["boris", "2", "rk4", "yhe2", "chin_2a", "chin_2b", 
              "suzuki4", "yoshida4", "prk4", "prk6", "ss6_7", "ss6_9",
              "ss8_15", "ss8_17"]

energy = []

for algorithm in algorithms:

    energies = []

    for i in range(2,7):
        for j in range(4):
    
            filepath = '1e-' + str(i) + '/' + algorithm + 'mag' + str(j) + '.h5'
            f=h5py.File(filepath, 'r')

            # get energies
            ekin=f['kinetic_energy'][-1]
            epot=f['potential_energy'][-1]
            esum = ekin+epot

            #dE/E
            energy0 = f['parameters'].attrs.get('energy')
            dE = np.absolute(esum - energy0)

            energydiff = dE/energy0
            energies.append(energydiff)
            f.close()
            
    energy.append(energies) 
    
file = open('energyerror_soft.csv','wb')

with file:
    writer = csv.writer(file)
    for algorithm in range(len(energy)):    
        writer.writerow(energy[algorithm])        

file.close()

