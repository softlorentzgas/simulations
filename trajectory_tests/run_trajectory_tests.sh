#!/bin/bash

energy=0.5
#magfield=("0.2" "0.335" "0.5" "1")
magfield=("1.2" "1.5" "2")
maxt=50

m=0

while (( $m < 3 ))
do

	#First run to determine possible x0, y0, vx0 and vy0
	bill2d --billtype 4 --save-trajectory --save-velocity --save-energies --table 0 --magnetic-field ${magfield[$m]} --potential-strength 1.5 --periodic --unit-cell 2.5 --soft-lorentz-gas --soft-lorentz-radius 1 --soft-lorentz-sharpness 0.1 --soft-lorentz-lattice-sum-truncation 4 --propagator suzuki4 --max-t ${maxt} --energy ${energy} --output 0 --delta-t 1e-3 

	#extract initial position and velocity and write to initial
	python3 ../../get_initial_position.py

	algo=("boris" "chin_2a" "chin_2b" "yhe2" "rk4" "prk4" "prk6" "suzuki4" "yoshida4" "2" "ss6_7" "ss6_9" "ss8_15" "ss8_17")

	a=0
	while (( $a < 14 ))
	do
        	b=2
        	echo ${algo[${a}]}
        	while (( $b <= 7 ))
        	do
			while ((`pgrep -c bill2d`>19))
			do
				sleep 1
			done
			bill2d --billtype 4 --save-trajectory --save-velocity --save-energies --table 0 --sparsesave $((10**((${b})-2))) --magnetic-field ${magfield} --potential-strength 1.5 --periodic --unit-cell 2.5 --soft-lorentz-gas --soft-lorentz-radius 1 --soft-lorentz-sharpness 0.1 --soft-lorentz-lattice-sum-truncation 4 --propagator ${algo[${a}]} --max-t ${maxt} --energy ${energy} --initial-position initial --output 0 --delta-t 1e-${b} --savepath 1e-${b}/${algo[${a}]}mag${m}.h5 &

                
                	echo 1e-${b}/${algo[${a}]}mag${m}.h5
			(( b+=1))
        	done
        	((a+=1))
	done
	((m+=1))

done 
while ((`pgrep -c bill2d`>0))
do
	sleep 1 
done

rm initial

echo Done! 
