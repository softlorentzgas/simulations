# -*- coding: utf-8 -*-
"""
Created on Thu Jun 21 15:50:24 2018

@author: Ariane
"""

import h5py

f=h5py.File('data/bill2d.h5','r')

traj_data=f["/trajectories/particle1"]
x = traj_data[0][0]
y = traj_data[0][1]
vel_data=f["/velocities/particle1"]
vx = vel_data[0][0]
vy = vel_data[0][1]

f.close() 



s = " "
vec = [str(x), str(y), str(vx), str(vy)]
initialdata = s.join( vec )

print(initialdata)

g = open('initial','w')
g.write(initialdata)

g.close()
